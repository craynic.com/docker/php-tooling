Extended version of the official PHP image with several tools added:

* Composer
* cURL, wget
* phpUnit
* xDebug

Purpose of this image is for building PHP based projects in Gitlab's CI/CD pipeline. 

Check https://hub.docker.com/_/php for information about the original image.

Usage:

1. Run `bash` in the latest version of the image:

```bash
docker run -it --rm --name php-tooling \
registry.gitlab.com/craynic.com/docker/php-tooling:latest \
bash
```

2. Run `bash` in with PHP 7.3:

```bash
docker run -it --rm --name php-tooling \
registry.gitlab.com/craynic.com/docker/php-tooling:7.3 \
bash
```
