FROM php:7.4
MAINTAINER Aleš Krajník <ales@krajnik.family>

ARG DEBIAN_FRONTEND=noninteractive

COPY files/usr/local/etc/php/conf.d/craynic.ini /usr/local/etc/php/conf.d/craynic.ini

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y \
        git \
        wget \
        unzip \
        libcurl4-gnutls-dev \
    && docker-php-ext-install curl \
    && wget https://composer.github.io/installer.sig -O - -q | tr -d '\n' > installer.sig \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php --install-dir /usr/local/bin \
    && ln -rs /usr/local/bin/composer.phar /usr/local/bin/composer \
    && php -r "unlink('composer-setup.php'); unlink('installer.sig');" \
    && curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar \
    && chmod +x /usr/local/bin/phpunit \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug

